#include <stdio.h>
#include <sys/types.h>
#include <psxetc.h>
#include <psxgte.h>
#include <psxgpu.h>

#include "Data/include/menuScreen.h"
#include "Data/include/gameScreen.h"
#include "Data/include/overScreen.h"

StateMachine GameMachineState;

DISPENV mainDisplay[2];
DRAWENV mainDraw[2];
int db;
// Init function
void init(void)
{
	// This not only resets the GPU but it also installs the library's
	// ISR subsystem to the kernel
	ResetGraph(0);
	
	// Define display environments, first on top and second on bottom
	SetDefDispEnv(&mainDisplay[0], 0, 0, 320, 240);
	SetDefDispEnv(&mainDisplay[1], 0, 240, 320, 240);
	
	// Define drawing environments, first on bottom and second on top
	SetDefDrawEnv(&mainDraw[0], 0, 240, 320, 240);
	SetDefDrawEnv(&mainDraw[1], 0, 0, 320, 240);
	
	// Set and enable clear color
	setRGB0(&mainDraw[0], 0, 0, 0);
	setRGB0(&mainDraw[1], 0, 0, 0);
	mainDraw[0].isbg = 1;
	mainDraw[1].isbg = 1;
	
	// Clear double buffer counter
	db = 0;
	
	// Apply the GPU environments
	PutDispEnv(&mainDisplay[db]);
	PutDrawEnv(&mainDraw[db]);
	
	// Load test font
	FntLoad(960, 0);
	
	// Open up a test font text stream of 100 characters
	FntOpen(0, 8, 320, 224, 0, 100);
}

// Display function
void display(void)
{
	// Flip buffer index
	db = !db;
	
	// Wait for all drawing to complete
	DrawSync(0);
	
	// Wait for vertical sync to cap the logic to 60fps (or 50 in PAL mode)
	// and prevent screen tearing
	VSync(0);

	// Switch pages	
	PutDispEnv(&mainDisplay[db]);
	PutDrawEnv(&mainDraw[db]);
	
	// Enable display output, ResetGraph() disables it by default
	SetDispMask(1);
	
}

// Main function, program entrypoint
int main(int argc, const char *argv[])
{

	// Init stuff	
	init();

	// Init menu State
	StateMachineStart(&GameMachineState, &GameState);
	
	// Main loop
	while(1)
	{
		StateMachineUpdate(&GameMachineState);
		StateMachineDraw(&GameMachineState, &mainDraw, db);
		
		display();		
	}
	
	return 0;
}
