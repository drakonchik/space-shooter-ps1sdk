#ifndef GAME_STATE
#define GAME_STATE

#include <sys/types.h>
#include <stdio.h>
#include <psxgte.h>
#include <psxgpu.h>

#include "stateManager.h"

void GameStart();
void GameUpdate();
void GameDraw(DRAWENV *mainDraw[2], int flicker);
void GameEnd();

extern StateManager GameState;

#endif
