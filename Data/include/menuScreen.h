#ifndef MENU_STATE
#define MENU_STATE
#include <sys/types.h>
#include <stdio.h>
#include <psxgte.h>
#include <psxgpu.h>

#include "stateManager.h"

void MenuStart();
void MenuUpdate();
void MenuDraw(DRAWENV *mainDraw[2], int flicker);
void MenuEnd();

extern StateManager MenuState;

#endif
