#ifndef OVER_STATE
#define OVER_STATE
#include <sys/types.h>
#include <stdio.h>
#include <psxgte.h>
#include <psxgpu.h>

#include "stateManager.h"

void OverStart();
void OverUpdate();
void OverDraw(DRAWENV *mainDraw[2], int flicker);
void OverEnd();

extern StateManager OverState;

#endif
