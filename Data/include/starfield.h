#ifndef STARFIELD
#define STARFIELD

typedef struct
{
	float posX;
	float posY;
	float pos2X;
	float pos2Y;
} Starfield;

void InitializeStarfield();

void ScrollBackground();

void DrawBackground(DRAWENV *mainDraw[2], int flicker);

#endif
