#include "include/starfield.h"
#include <stdlib.h>

Starfield stars;
 
TIM_IMAGE sfImage;

void InitializeStarfield()
{
	char* path = "Graphics/Background/SF.tim";
	
	GetTimInfo( path, &sfImage);
	
	LoadImage(sfImage.prect, sfImage.paddr);
	
	DrawSync(0);
	
	if( sfImage.mode & 0x8 ) 
	{
		//Upload CLUT if present 
		LoadImage(sfImage.crect,sfImage.caddr);
	}
	
	DrawSync(0);
	
	stars.posX = 0.0f;
	stars.posY = 0.0f;
	stars.pos2X = 0.0f;
	stars.pos2Y = -512.0f;
}

void ScrollBackground(GSGLOBAL *gsGlobal)
{
	stars.posY+= 4.0f;	
	stars.pos2Y+= 4.0f;	
	//printf("Y1 = %.2f Y2 = %.2f\n", stars.posY, stars.pos2Y);	
	// If Stars image reaches end of screen.	
	if(stars.posY > 512.0f)
	{
		stars.posY = -512.0f;
	}
	
	if(stars.pos2Y > 512.0f)
	{
		stars.pos2Y = -512.0f;
	}
}

void DrawBackground(DRAWENV *mainDraw[2], int flicker)
{

}
