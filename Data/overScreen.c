#include "include/gameScreen.h"
#include "include/menuScreen.h"
#include "include/overScreen.h"

extern StateMachine GameMachineState;

void OverStart()
{

}

void OverUpdate()
{

}

void OverDraw(DRAWENV *mainDraw[2], int flicker)
{

}

void OverEnd()
{

}

StateManager OverState =
{
	OverStart,
	OverUpdate,
	OverDraw,
	OverEnd
};
