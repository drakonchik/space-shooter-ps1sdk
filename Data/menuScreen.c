#include "include/gameScreen.h"
#include "include/menuScreen.h"
#include "include/overScreen.h"

TIM_IMAGE tim;

extern StateMachine GameMachineState;

// Initializer
void MenuStart()
{
	
}

// Update
void MenuUpdate()
{

}

// Draw
void MenuDraw(DRAWENV *mainDraw[2], int flicker)
{

}

// Destructor of sorts
void MenuEnd()
{

}

StateManager MenuState =
{
	MenuStart,
	MenuUpdate,
	MenuDraw,
	MenuEnd
};
