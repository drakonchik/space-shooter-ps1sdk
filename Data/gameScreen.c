#include "include/gameScreen.h"
#include "include/menuScreen.h"
#include "include/overScreen.h"
#include "include/starfield.h"

extern StateMachine GameMachineState;

void GameStart()
{
	InitializeStarfield();
}

void GameUpdate()
{

}

void GameDraw(DRAWENV *mainDraw[2], int flicker)
{
	DrawBackground(&mainDraw, flicker);
}

void GameEnd()
{

}

StateManager GameState =
{
	GameStart,
	GameUpdate,
	GameDraw,
	GameEnd
};
